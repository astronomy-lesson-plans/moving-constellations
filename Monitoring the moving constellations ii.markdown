Review a research report. Your task is to improve a report similar to yours. Answer the questions to determine the quality of the report

Formulate a Question, Pursue Evidence, and Justify Your Conclusion

Your task is to design an answerable research question, propose a plan to pursue evidence, collect data using Stellarium (or other program pre-approved by your teacher), and create an evidence-based conclusion about motion or position of a constellation in the sky.

Research report:

Specific reseach question:

DURING WHICH SEASON IS THE CONSTAELLATAION ORIong high IN THe SOUtahern SKY JUSTA AFTER SUNSET.?

step bystep procedure to collect evidence

Using Stellarium or your paper planisphere to make observations:
1) Chose the first day of the month to observe
2) On each observation day, just after sunset, determine if Orion is visible and what part of the sky it is located.
3) Repeat the observatin once a month for a year.


Data table and results

| Date | Visible | Location |
| ---- | ------- | -------- |
| 4/1/09 | yes | high SW |
| 5/1/09 | yes | low W   |
| 6/1/09 | no |  |
| 7/1/09 | no |  |
| 8/1/09 | no |  |
| 9/1/09 | no |  |
| 10/1/09 | no |  |
| 11/1/09 | no |  |
| 12/1/09 | no |  |
|  1/1/10 | yes | low E |
|  2/1/10 | yes | high SE |
|  3/1/10 | yes | high S |

Evidence based conclusion

From the evidence above, we can see that the constellation Orion appears to move from low in the eastern sky to low in the western sky from January to May.



Case Study Report

Your task is to design an answerable research question, propose a plan to pursue evidence, collect data using Stellarium (or other program pre-approved by your teacher), and create an evidence-based conclusion about motion or position of a constellation in the sky.

Research report:

Specific reseach question:
List the things you want to observe to answer this question:



Step bystep procedure to collect evidence: Is the plan going to give the right observations to answer the question




Data table and results



Evidence based conclusion



