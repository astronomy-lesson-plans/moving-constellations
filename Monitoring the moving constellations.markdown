Monitoring the moving constellations

Big idea: Sky objects have properties, locations, and predictable patterns of movement that can be obserev and described.

Goal: You will conduct a series of inquiries about the position and motion of constellations using the prescribed computer siulations and learn how different stars are visible at different times of the year in different locations in the sky.

Phase I -- Exploration:

1. Go to https://www.stellarium-web.org

2. ![img](images/location.png) <img style="float: left;" src="images/location.png">  Share your location!  The sky simulator will automagically simulate the sky for your current date and time. It will be close enough if the pin is somewhere in the Portland Metro area.

3. ![img](images/realtime.png) <img style="float: left;" src="images/realtime.png"> Stellarium-web switched the time to night! We want right now! Click on the clock on the right hand side of the screen. Click on the clock icon to reset the time to "Right Now."

3. ![img](images/atmosphere.png) <img style="float: left;" src="images/atmosphere.png"> Earth's atmosphere scatters (speads) blue light from our Sun's light. We can turn off the atmosphere. This would be our view if Earth had no air. Tell stellarium to turn-off the atmosphere simulator. 

4. What is now visisble during the day that wasn't visible before?

    - ___________________ are now visible.

4. ![img](images/constellations.png) <img style="float: left;" src="images/constellations.png"> Constellations are imaginary shapes made from patterns in the stars. Stellarium can draw labels and lines in the sky. Turn these on. What constellation is the sun in right now?
    - The sun is in the constllation of ______________________________.

5. Increase the time by one hour. You may need to click on the clock to get back to the time controls. Which way does the Sun seem to move?

    - The Sun moved _____________(North, South, East, West).

6. Where is this direction in our classroom:

    - ________________________________________________________________________________________.

7. Which constellation is the Sun now in?

    - The sun is ________________________________________________________.

8. Advance the time to Sunset -- when the Sun starts to go below ground. About what time will this happen today?

    - The Sun will set around _____:______ _____ today.

9. What constellation is the Sun in at Sunset?

    - During sunset the Sun is in ________________________________________________________.

10. Move the time forward to tomorrow's sunrise. About what time will the sun rise tomorrow?

    - Tomorrow the ________________________________________________________________________________.

11. What constellation is the Sun in?

    - (Complete sentence foo!) <br /><br /><br />

12. Write a sentence that describes how the sun and stars move through the sky:

    - <br /><br /><br />

\newpage

Phase II --  Does the evidence match the conclusons?

Here is your research question: "What direction does the Sun move compared to the background stars and constellations?"

- Set Stellarium to noon today. If you could see the stars hidden behind the gorgious Sun, serving brilliant glowing star realness, which constellation would the sun be in?

- What constellation is the Sun in at noon tomorrow?

- What constellation is the Sun in at noon, 7 days from now?

- What constellation is the Sun in at noon, two weeks from now?

- What constellation is the Sun in at noon, three weeks from now?

- What constellation is the sun in at noon, one month from now?

- Two months?

- Three months?

- Six months?

- Nine months?

- One year?

- Two years?

- Four years?

Here is a claim: "The constellations seem to slowly drift westward compared to the Sun. The sun crossing constellations at a rate once per week."

Do you agree or disagree with this sentince:

I _________________(agree/disagree) with the claim. _________________________________________________________ (What is one bit of evidence from Stellarium that supports your judgement) ________________(supports/does not support) the claim. More evidence that ____________________ (supports/does not support) the claim is ______________________________________________________.

Part III - Conclusions based on evidence -- and reading a data table

Orion is a very prominant constellation in the winter time, and is hidden by the Sun in the summer time. Using the data table below, write two generalizations/conlusions on when Orion is directly South in the sky. What is the evidence you used to make these conclusions?

| Date  | Time (directly south) | Azimuth1. |
| ----- | --------------------- | ------- |
| October 1 | 5:00 am | 180 |
| November 1 | 3:00 am | 180 |
| December 1 | 1:00 am | 180 |
| January 1  | 11:00 pm | 180 |
| Feburary 1 | 9:00 pm  | 180 |

1.Direction in the sky. North is 0. East is 90. South is 180. West is 270.

One thing I notice about Orion is _______________________________________________________________________. The evidence for this in the table is __________________________________. Another thing Orion does during the year is ___________________________________. The table shows this by _______________________________.


Part IV - What evidence do you need?
Imagine you convinced your friends that thier horoscopes are not what they think they are and are getting tired of looking up all of thier birthdays (and their mother's, father's, and exes) in Stellarium. What evidence do you need to collect to make a new horoscope calendar -- showing what days the sun is in different constellations?

To find the first day the Sun is in Virgo I would _________________________________________________________________________________.
To find the last day the Sun is in Virgo I would __________________________________________________________________________________.
To find what the next constellation is I would ____________________________________________________________________________________.
Then ______________________________________________________________________________________________________________________________.
(Use more sentences if you need to)

Part V -- Formulate a Question, Persue Evidence, and Justify Your Conclusion

Your task is to design an answerable research question about the motion of constellations or planets, propose a plan on getting the evidence, collect the evidence using Stellarium, and write a conclusion that answers your question using the evidence you collected. You may use Stellarium, your paper star wheel, or another program (with my approval).

Da REPORT

Specific question:
(Word bank to help you make a question: Retograde, azimuth, zenith, rise, set, ecliptic, right assension, declination, day, month, year, midnight)




Write a step-by-step procedure on how you collected evidence (What days/times are you setting, are you watching the Sun, a star, contellation boundary, planets?)





Data table/results:






Evidence based conclusion

The answer to the research question is _______________________________________. The evidence for this is ___________________________________. Other evidence includes ______________________________________________________, and ________________________________________________________.





Phase VI - Summary
Write a 50 word summary, in your own words, that describes which constellations are visible at night and how this changes over the night and year. Feel free to draw and label sketches to illustrate your response.